# like = "likes()"
# like = "likes(\"Ann\")"
# like = "likes(\"Ann\", \"Alex\")"
# like = "likes(\"Ann\", \"Alex\", \"Mark\")"
# like = "likes(\"Ann\", \"Alex\", \"Mark\", \"Max\")"
like = "likes(\"Ann\", \"Alex\", \"Mark\", \"Max\")"
if like.find("likes(") != -1 and len(like) > 7:
    str1 = like[6:len(like) - 1]
    list1 = str1.split(",")
    if len(list1) == 1:
        print(f"{list1[0]} likes this")
    elif len(list1) == 2:
        print(f"{list1[0]} and {list1[1]} like this")
    elif len(list1) == 3:
        print(f"{list1[0]}, {list1[1]} and {list1[2]} like this")
    else:
        print(f"{list1[0]}, {list1[1]} and {len(list1) - 2} others like this")
else:
    print("no one likes this")
