from pages.base_page import BasePage
from locators.MainPageLocators import MainPageLocators


class MainPage(BasePage):

    def open_login_page(self):
        sing_in_button = self.find_element(
            MainPageLocators.LOCATOR_SING_IN_BUTTON)
        sing_in_button.click()

    def open_cart_page(self):
        cart_link = self.find_element(MainPageLocators.LOCATOR_CART_LINK)
        cart_link.click()

    def should_be_t_shirts(self):
        t_shirts = self.find_element(
            MainPageLocators.LOCATOR_T_SHIRTS)
        t_shirts.click()

    def should_be_add_to_card(self):
        t_shirts = self.find_element(
            MainPageLocators.LOCATOR_T_SHIRTS_ADD)
        t_shirts.click()

    def should_be_checkout(self):
        checkout = self.find_element(
            MainPageLocators.LOCATOR_CHECKOUT)
        checkout.click()
