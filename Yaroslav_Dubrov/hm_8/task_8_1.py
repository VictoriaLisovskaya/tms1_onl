def typed(type):

    def decorator(func):

        def wrapper(*args):

            # if type = string, we convert each element into string
            if type == "str":
                args = map(str, args)

            # if type = int => integer
            elif type == "int":
                args = map(int, args)

            # if type = float => float
            elif type == "float":
                args = map(float, args)

            # if type other than str/int/float we show error msg
            else:
                print("Incorrect values")
            return func(*args)
        return wrapper
    return decorator


# Check results
@typed(type="str")
def add(a, b):
    return a + b


print(f"Result of adding is {type(add(5, 7))}, value: {add(5, 7)}")


@typed(type="int")
def add(a, b, с):
    return a + b + с


print(f"Result of adding is {type(add(5, 7, 7))}, value: {add(5, 7, 7)}")


@typed(type="float")
def add(a, b, с):
    return a + b + с


print(f"Result of adding is {type(add(5, 7, 9))}, value: {add(5, 7, 9)}")
