a = ("Ann")  # I wondered that it's not a tuple :)

# in case if a is string, I convert it to list
if type(a) == str:
    def Convert(string):
        li = list(string.split(" "))
        return li
    a_conv = Convert(a)
    print(f"{a} likes this")

# if a is tuple, work with number of people's names in a tuple
else:
    if len(a) == 0:
        print("no one likes this")
    elif len(a) == 2:
        print(f"{' and '.join(a)} likes this")
    elif len(a) == 3:
        short_a = a[0:2]
        rest_a = a[2]
        print(f"{', '.join(short_a)} and {rest_a} likes this")
    elif len(a) >= 4:
        short_a = a[0:2]
        people_num = len(a) - 2
        print(f"{', '.join(short_a)} and {people_num} others likes this")
