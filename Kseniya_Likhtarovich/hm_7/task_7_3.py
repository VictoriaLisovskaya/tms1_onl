"""
Шифр Цезаря.

Напишите функцию для зашифровки и дешифровки.
Шифр работает на нескольких языках; сохраняет регистр, знаки препинания.
"""

import string

chs = string.ascii_letters
abc = len(chs)

ru_low_lst = [chr(i) for i in range(ord("а"), ord("я") + 1)]
ru_up_lst = [chr(i) for i in range(ord("А"), ord("Я") + 1)]
ru_low_lst.insert(6, "ё"), ru_up_lst.insert(6, "Ё")
chs_ru = "".join(ru_low_lst + ru_up_lst)
abc_ru = len(chs_ru)


def encode_decode(text, shift, lang="en", decrypt=False):
    """Шифрует/Дешифрует текст."""
    if lang == "en":
        if decrypt:
            shift = abc - shift
        enc_dec = str.maketrans(chs, chs[shift:] + chs[:shift])
        enc_dec_string = text.translate(enc_dec)
        return enc_dec_string

    if lang == "ru":
        if decrypt:
            shift = abc_ru - shift
        enc_dec_ru = str.maketrans(chs_ru, chs_ru[shift:] + chs_ru[:shift])
        enc_dec_string_ru = text.translate(enc_dec_ru)
        return enc_dec_string_ru


print(encode_decode("Hello World!", 3,))
print(encode_decode("This is a test String 4", 5))
print(encode_decode("Khoor, Zruog!", 3, decrypt=True))
print(encode_decode("Ymnx nx f yjxy Xywnsl", 5, decrypt=True))
print(encode_decode("Привет Мир!", 3, "ru"))
print(encode_decode("Тулезх Плу!", 3, "ru", decrypt=True))
