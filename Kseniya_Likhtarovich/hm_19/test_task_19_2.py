import pytest
from main_task_19_2 import game


class TestGame:

    @pytest.mark.smoke
    @pytest.mark.parametrize("player1, player2, answer",
                             [("1235", "9876", "Быки: 0, Коровы: 0."),
                              ("3257", "0235", "Быки: 1, Коровы: 2."),
                              ("6285", "6285",
                               "Вы победитель по жизни! Быки: 4, Коровы: 0")])
    def test_game_1(self, player1, player2, answer):
        assert game(player1, player2) == answer

    @pytest.mark.regression
    @pytest.mark.parametrize("player1, player2, answer",
                             [("2468", "8642", "Быки: 0, Коровы: 4."),
                              ("4967", "9467", "Быки: 2, Коровы: 2."),
                              ("2348", "2148", "Быки: 3, Коровы: 0.")])
    def test_game_2(self, player1, player2, answer):
        assert game(player1, player2) == answer

    @pytest.mark.xfail(reason="Не реализовано.")
    @pytest.mark.parametrize("player1, player2", [("628", "6254"),
                                                  ("62835", "6254"),
                                                  ("6623", "2468"),
                                                  ("63 95", "6395"),
                                                  ("91a3", "2498"),
                                                  ("23&5", "1287"),
                                                  ("", "8392")])
    def test_game_3(self, player1, player2, message):
        assert game(player1, player2) == message
