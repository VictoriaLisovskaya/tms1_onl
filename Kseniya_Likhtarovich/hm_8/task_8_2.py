"""
На вход подаётся некоторое количество целых чисел.

Выведите их в порядке лексикографического возрастания названий чисел.
"""

number_names = {
    0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five',
    6: 'six', 7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven',
    12: 'twelve', 13: 'thirteen', 14: 'fourteen', 15: 'fifteen',
    16: 'sixteen', 17: 'seventeen', 18: 'eighteen', 19: 'nineteen'
}


def decorator(func):
    """Отбирает и сортирует ключи."""
    def wrapper(keys):
        keys = [int(i) for i in keys.split()]
        dct = {i: number_names.get(i) for i in keys}
        sort_val = sorted(dct, key=number_names.get)
        string = " ".join(map(str, sort_val))
        func(string)
    return wrapper


@decorator
def sorted_values(keys):
    print(keys)


sorted_values("1 2 3")
sorted_values("2 5 8")
