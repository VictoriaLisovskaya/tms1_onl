from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

browser = webdriver.Chrome('./chromedriver.exe')
browser.implicitly_wait(5)
browser.get("http://the-internet.herokuapp.com/dynamic_controls")


def test_wait():
    checkbox = browser.find_element_by_css_selector('[type="checkbox"]')
    remove = browser.find_element_by_css_selector('[onclick="'
                                                  'swapCheckbox()"]')
    remove.click()
    message = WebDriverWait(browser, 5).until(
        EC.visibility_of_element_located((By.ID, "message")))
    assert message.text == "It's gone!"
    WebDriverWait(browser, 1).until(EC.invisibility_of_element(checkbox))
    input_field = browser.find_element_by_css_selector('[type="text"]')
    assert input_field.get_property("disabled")
    enable_field = browser.find_element_by_css_selector('[onclick='
                                                        '"swapInput()"]')
    enable_field.click()
    disable_message = WebDriverWait(browser, 5).until(
        EC.visibility_of_element_located((By.ID, "message")))
    assert disable_message.text == "It's enabled!"
    if input_field.is_enabled():
        pass
    browser.quit()
