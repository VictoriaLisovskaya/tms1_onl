from operator import attrgetter


class Flower:

    def __init__(self, flower_name: str, colour: str, wilt_time: int,
                 stem_length: int):
        """This method creates flower"""
        self.flower_name = flower_name
        self.colour = colour
        self.wilt_time = wilt_time
        self.stem_length = stem_length

    def __str__(self):
        """This method returns flower info"""
        return f"Flower: {self.flower_name}, {self.colour}, " \
               f"wilting {self.wilt_time}h, length: " \
               f"{self.stem_length}cm"


class FlowerShop:

    def __init__(self):
        self.bouq = []
        self.bouq_flowers = []

    def add_flower(self, fl, qty: int, price: int):
        """This method adds flower to bouquet"""
        self.bouq.append([fl.flower_name, price, fl.wilt_time,
                          fl.colour, fl.stem_length, qty])
        self.bouq_flowers.append(fl)

    def bouquet(self):
        """This method returns flowers in bouquet, it's price and it's
        wilting time"""
        name_list = []
        price_list = []
        avg_wilt_t = []
        for n in self.bouq:
            name_list.append(n[0])
            price_list.append(int(n[1]) * n[5])
            avg_wilt_t.append(int(n[2]))
        return f"Flowers in Your bouquet: {', '.join(name_list)}" \
               f". It's price: {sum(price_list)} BYN. Bouquet's " \
               f"wilting time is " \
               f"{int(sum(avg_wilt_t) / len(self.bouq))}h"

    def sort_by_attrib(self, name):
        """This method sorts flowers in bouquet according attribute"""
        sorted_list = []
        try:
            for si in sorted(self.bouq_flowers, key=attrgetter(name)):
                sorted_list.append(str(si))
        except AttributeError:
            return f"'Flower' object has no attribute '{name}'"
        else:
            return sorted_list

    def fl_in(self, param):
        """This method allows to check is flower in bouquet according
         received parameter"""
        flowers_list = []
        for k in self.bouq:
            if param in k:
                flowers_list.append(k[0])
        if len(flowers_list) > 0:
            return f"Here is the list of flowers in Your bouquet according" \
                   f" parameter: {', '.join(flowers_list)}."
        return f"No flowers in Your bouquet according parameter '{param}'"


chamomile = Flower('Chamomile', 'White', 48, 20)
daisy = Flower('Daisy', 'Yellow', 24, 10)
rose = Flower('Rose', 'Cream', 72, 40)
orchid = Flower('Orchid', 'White', 164, 20)

bouquet_1 = FlowerShop()
bouquet_1.add_flower(chamomile, 5, 4)
bouquet_1.add_flower(daisy, 20, 1)
bouquet_1.add_flower(rose, 10, 7)
bouquet_1.add_flower(orchid, 1, 100)
print(bouquet_1.bouquet())
print(bouquet_1.fl_in('White'))
print(bouquet_1.sort_by_attrib('stem_length'))
