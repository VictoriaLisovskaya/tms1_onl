from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, browser):
        self.browser = browser
        self.base_page = "http://automationpractice.com/index.php"

    def open_main_page(self):
        self.browser.get(self.base_page)

    def find_element(self, locator: tuple, time=10):
        return WebDriverWait(self.browser, time).until(
            EC.presence_of_element_located(locator),
            message=f"Can't find element by locator {locator}"
        )

    def find_elements(self, locator: tuple, time=10):
        return WebDriverWait(self.browser, time).until(
            EC.invisibility_of_element(locator),
            message=f"Element by locator {locator} is present"
        )
