# Calculator v0.2
def define_numbers(list_args: list, operation):
    """This function calculates arguments according arithmetic sign"""
    ind = list_args.index(f"{operation}")
    if operation == '**':
        res = int(list_args[ind - 1]) ** int(list_args[ind + 1])
    elif operation == '*':
        res = int(list_args[ind - 1]) * int(list_args[ind + 1])
    elif operation == '/':
        res = int(list_args[ind - 1]) / int(list_args[ind + 1])
    elif operation == '+':
        res = int(list_args[ind - 1]) + int(list_args[ind + 1])
    elif operation == '-':
        res = int(list_args[ind - 1]) - int(list_args[ind + 1])
    else:
        res = list_args
    del list_args[ind - 1: ind + 2]
    list_args.insert(ind - 1, res)
    return list_args


def dec_calculation(func):
    def wrapper(user_input):
        """This function calculates user's expression with integer numbers
        and returns integer result"""
        if type(user_input) != list:
            user_input = user_input.split()
        # Check for operation priority
        for item in user_input:
            # Exponentiation
            if item == '**':
                define_numbers(user_input, '**')
            # Multiplication
            elif item == '*' and '**' not in user_input:
                define_numbers(user_input, '*')
            # Division
            elif item == '/' and '**' not in user_input:
                define_numbers(user_input, '/')
            # Addition
            elif item == '+' and '**' not in user_input \
                    and '*' not in user_input and '/' not in user_input:
                define_numbers(user_input, '+')
            elif item == '-' and '**' not in user_input \
                    and '*' not in user_input and '/' not in user_input:
                define_numbers(user_input, '-')
            else:
                continue
            # Another cycle if result wasn't get
            if len(user_input) != 1:
                wrapper(user_input)
            else:
                func(user_input[0])
    return wrapper


@dec_calculation
def received_expression(result):
    print(f"Integer result: {int(result)}")


received_expression(input("Enter Your expression with spaces. "
                          "Use integers. Example: 2 + 3 * 4: "))
