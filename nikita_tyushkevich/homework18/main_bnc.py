from collections import Counter


def inp_check(players_try):
    answer1 = 8465
    answer2 = str(answer1)
    correct_digits_num = 0
    correct_places = 0
    # Check 4 digits in input
    if len(players_try) != 4:
        print("Incorrect number")
        return False
    # Check fo numeric input
    if not players_try.isnumeric():
        print("Spacial chars or letters")
        return False
    # Check that values are unique
    for p in Counter(players_try).values():
        if p != 1:
            print("Not unique values")
            return False
    # Search for intersecting values
    if answer2 != players_try:
        for i in players_try:
            if i == answer2[0] or i == answer2[1] or i == answer2[2] \
                    or i == answer2[3]:
                correct_digits_num += 1
        # Search for digits at the same places in numbers
        for b in range(len(players_try)):
            if b == 0 and answer2[0] == players_try[0] \
                    or b == 1 and answer2[1] == players_try[1] \
                    or b == 2 and answer2[2] == players_try[2] \
                    or b == 3 and answer2[3] == players_try[3]:
                correct_places += 1
                correct_digits_num -= 1
        # Result for Cows and Bulls
        print(f"Cows: {correct_digits_num}, Bulls: {correct_places}.")
        return correct_digits_num, correct_places
    # Number was guessed
    elif answer2 == players_try:
        print("You're right!")
        return True


pl_try = '8465'
inp_check(pl_try)
