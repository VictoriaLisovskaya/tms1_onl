import re

print('Выбирите операцию: \n1. Сложение \n2. Вычетание '
      '\n3. Умножение \n4. Деление')
choice_number = input('Введите номер пункта меню: ')
number_1 = input('Введите первое число: ')
number_2 = input('Введите второе число: ')


def calculator(number_menu, one_number, two_number):
    """Валидация полей"""
    result_menu = bool(re.findall(r'\D', number_menu))
    result_one_number = bool(re.findall(r"\D", one_number))
    result_two_number = bool(re.findall(r"\D", two_number))
    if result_menu and result_one_number and result_two_number:
        return print('Только числа!')

    """Преобразовываем к числу внутри функции"""
    converting_menu = int(number_menu)
    converting_one_number = int(one_number)
    converting_two_number = int(two_number)

    """Блок с вычеслениями"""
    if converting_one_number == 0 and converting_two_number == 0:
        return print('Деление на 0')
    elif converting_menu == 1:
        return print(converting_one_number + converting_two_number)
    elif converting_menu == 2:
        return print(converting_one_number - converting_two_number)
    elif converting_menu == 3:
        return print(converting_one_number * converting_two_number)
    elif converting_menu == 4:
        quotient = converting_one_number / converting_two_number
        residue = converting_one_number % converting_two_number
        return print(f'Частное: {int(quotient)}, Остаток: {residue}')


calculator(choice_number, number_1, number_2)
