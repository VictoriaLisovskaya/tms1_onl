# Задание 2
var_strng = 'ivan'
var_strng_lst = ['ivan', 'stepan', 'test']
var_ing = 'ing'
print(f'{var_strng}{var_ing}')  # Работает для одной строки
print('ing '.join(var_strng_lst))  # Не работает для последнего индекса

for name in var_strng_lst:  # Работает для всех значений в списке
    print(name + var_ing, end=' ')
