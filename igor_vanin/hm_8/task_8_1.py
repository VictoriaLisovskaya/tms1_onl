def decorator_type(typer):
    def outer(func):
        def wrapper(*args):
            if typer == str:
                transform = map(str, args)
                return print(func(*transform))
            elif typer == int:
                transform = map(int, args)
                return print(func(*transform))
            elif typer == float:
                transform = map(float, args)
                return print(func(*transform))
        return wrapper

    return outer


@decorator_type(typer=str)
def add_two(a, b):
    return a + b


"""Tests"""
add_two("3", 5)
add_two(5, 5)
add_two('a', 'b')


@decorator_type(typer=int)
def add_three(a, b, c):
    return a + b + c


"""Test"""
add_three(5, 6, 7)
add_three("3", 5, 0)


@decorator_type(typer=float)
def add_three(a, b, c):
    return a + b + c


"""Test"""
add_three(0.1, 0.2, 0.4)
