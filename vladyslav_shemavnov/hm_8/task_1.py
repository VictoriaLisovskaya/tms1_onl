def typed(type):
    def decorator(func):
        def wrapper(*args):
            new_args = []
            for i in args:
                if type == 'str':
                    new_args.append(str(i))
                elif type == 'int':
                    new_args.append(int(i))
                else:
                    new_args.append(float(i))
            return func(*new_args)
        return wrapper
    return decorator


@typed(type='str')
def add(a, b):
    return a + b


print(add("3", 5))
print(add(5, 5))
print(add('a', 'b'))


@typed(type='int')
def add(a, b, c):
    return a + b + c


print(add(5, 6, 7))


@typed(type='int')
def add(a, b):
    return a + b


print(add("3", 5))


@typed(type='float')
def add(a, b, c):
    return a + b + c


print(add(0.1, 0.2, 0.4))
