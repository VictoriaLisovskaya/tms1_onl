a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
ab = {**a, **b}
for v in ab:
    if v in a and v in b:
        ab[v] = [a[v], b[v]]
    elif v in ab:
        ab[v] = [ab[v], None]
print(ab)
