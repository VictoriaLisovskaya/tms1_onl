import json


class Students:

    def __init__(self):
        with open("students.json", encoding='utf-8') as f:
            self.students_data = json.loads(f.read())

    def search_class_club(self, form, club):
        for i in self.students_data:
            if i["Class"] == form and i["Club"] == club:
                yield i

    def filter_gender(self, gender):
        for i in self.students_data:
            if i["Gender"] == gender:
                yield i

    def search_name(self, name):
        for i in self.students_data:
            if name in i["Name"]:
                return i


students = Students()

print("Поиск учащихся в одном классе, посещающих одну секцию:")
search_with_section = students.search_class_club("5a", "Football")
for i in search_with_section:
    print(i)

print("Фильтрация учащихся по их полу:")
search_with_gender = students.filter_gender("M")
for i in search_with_gender:
    print(i)

print("Поиск учащихся по имени(часть имени):")
print(students.search_name("Yuki"))
