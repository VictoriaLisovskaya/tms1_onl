import allure
from allure_commons.types import AttachmentType
from selenium import webdriver

from Rinat_Amirzhanov.homework22.pages.first_site_page import TestPage1Helper
from Rinat_Amirzhanov.homework22.pages.second_site_page import TestPage2Helper


class Application:

    def __init__(self):
        driver = webdriver.Chrome('./chromedriver.exe')
        self.driver = driver
        self.driver.maximize_window()
        self.first_site = TestPage1Helper(self)
        self.second_site = TestPage2Helper(self)

    def destroy(self):
        allure.attach(self.driver.get_screenshot_as_png(),
                      attachment_type=AttachmentType.PNG)
        self.driver.quit()
