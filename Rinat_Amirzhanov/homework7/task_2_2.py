def list_word(word):
    for i in word:
        if i != "the":
            yield i


text = "the quick brown fox jumps over the lazy dog"
new_text = text.split()
sorted_text = [i for i in list_word(new_text)]
print(len(sorted_text))
