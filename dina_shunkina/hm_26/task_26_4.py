"""Напечатайте заказы между 70001 и 70007(используйте between, and)"""

import mysql.connector as mysql

db = mysql.connect(
    host="localhost",
    user="root",
    passwd="",
    database="task_26"
)
cursor = db.cursor()
query = "SELECT * FROM orders WHERE ord_no BETWEEN 70001 AND 70007"
cursor.execute(query)
print(cursor.fetchall())
