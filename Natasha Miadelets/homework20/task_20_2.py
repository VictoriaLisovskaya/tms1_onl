import wikipediaapi


def test_wiki_page_api():
    word = 'Python (programming language)'
    en_wiki_page = wikipediaapi.Wikipedia('en')
    en_page = en_wiki_page.page(word)
    assert en_page.exists(), f"{word} doesn't exist"
    assert en_page.title == word, f"{en_page.title} is not eq {word}"
    print(f"Current page title is {en_page.title}")
