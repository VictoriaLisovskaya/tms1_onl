from selenium import webdriver
import time

url = "http://demo.guru99.com/test/newtours/register.php"
browser = webdriver.Chrome("./chromedriver 2")
first_name = "Natasha"
last_name = "Miadelets"
phone = "7331180"
email = "miadelets.natasha@gmail.com"
address = "street First, 5, 46"
city = "Minsk"
state = "Minsk"
postal_code = "12345"
country = "BELARUS"
username = "natasha.mi"
password = "123456"
conf_password = "123456"
browser.get(url)
first_name_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/" \
                   "tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/" \
                   "tr[5]/td/form/table/tbody/tr[2]/td[2]/input"
last_name_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/" \
                  "tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/" \
                  "tr[5]/td/form/table/tbody/tr[3]/td[2]/input"
phone_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/" \
              "tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/" \
              "form/table/tbody/tr[4]/td[2]/input"
email_xpath = "//*[@id=\"userName\"]"
address_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/" \
                "tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/" \
                "tr[5]/td/form/table/tbody/tr[7]/td[2]/input"
city_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/" \
             "tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/" \
             "form/table/tbody/tr[8]/td[2]/input"
state_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/" \
              "tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/" \
              "form/table/tbody/tr[9]/td[2]/input"
postal_code_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/" \
                    "tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/" \
                    "tr[5]/td/form/table/tbody/tr[10]/td[2]/input"
country_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/" \
                "tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/" \
                "form/table/tbody/tr[11]/td[2]/select"
username_xpath = "//*[@id=\"email\"]"
password_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/" \
                 "tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/" \
                 "form/table/tbody/tr[14]/td[2]/input"
conf_password_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/" \
                      "tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/" \
                      "tr[5]/td/form/table/tbody/tr[15]/td[2]/input"
send_button_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/" \
                    "tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/" \
                    "tr[5]/td/form/table/tbody/tr[17]/td/input"
f_l_name_text_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/" \
                      "tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/" \
                      "p[1]/font/b"
username_text_xpath = "/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/" \
                      "tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/" \
                      "p[3]/font/b"

first_name_input = browser.find_element_by_xpath(first_name_xpath)
first_name_input.send_keys(first_name)
last_name_input = browser.find_element_by_xpath(last_name_xpath)
last_name_input.send_keys(last_name)
phone_input = browser.find_element_by_xpath(phone_xpath)
phone_input.send_keys(phone)
email_input = browser.find_element_by_xpath(email_xpath)
email_input.send_keys(email)
address_input = browser.find_element_by_xpath(address_xpath)
address_input.send_keys(address)
city_input = browser.find_element_by_xpath(city_xpath)
city_input.send_keys(city)
state_input = browser.find_element_by_xpath(state_xpath)
state_input.send_keys(state)
postal_code_input = browser.find_element_by_xpath(postal_code_xpath)
postal_code_input.send_keys(postal_code)
country_dropdown = browser.find_element_by_xpath(country_xpath)
country_dropdown.send_keys(country)
username_input = browser.find_element_by_xpath(username_xpath)
username_input.send_keys(username)
password_input = browser.find_element_by_xpath(password_xpath)
password_input.send_keys(password)
conf_password_input = browser.find_element_by_xpath(conf_password_xpath)
conf_password_input.send_keys(conf_password)
time.sleep(2)
send_button_button = browser.find_element_by_xpath(send_button_xpath)
send_button_button.click()
time.sleep(2)
f_l_name_text = browser.find_element_by_xpath(f_l_name_text_xpath).text
username_text = browser.find_element_by_xpath(username_text_xpath).text

assert f_l_name_text == f"Dear {first_name} {last_name},",\
    f'**Successful Login** is not equal {f_l_name_text}'
assert username_text == f"Note: Your user name is {username}.",\
    f'**Successful Login** is not equal {username_text}'
browser.quit()
