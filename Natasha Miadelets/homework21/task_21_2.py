import json

with open('students.json', 'r') as f:
    data_students = json.load(f)


def search_by_class(file, key, value):
    list_of_students = []
    for i in file:
        for k, v in i.items():
            if k == key and v == value:
                list_of_students.append(i)

    return list_of_students


def search_by_club(file, key, value):
    list_of_students = []
    for i in file:
        for k, v in i.items():
            if k == key and v == value:
                list_of_students.append(i)

    return list_of_students


def search_by_gender(file, key, value):
    list_of_students = []
    list_name = []
    for i in file:
        for k, v in i.items():
            if k == key and v == value:
                list_of_students.append(i)
    for s in list_of_students:
        for k, v in s.items():
            if k == 'Name':
                list_name.append(v)

    return f'Name of students with {key} {value} - {list_name}'


def search_by_name(file, key, value):
    list_of_students = []
    for i in file:
        for k, v in i.items():
            if k == key and v.find(value) >= 0:
                list_of_students.append(i)

    return list_of_students


print(search_by_class(data_students, 'Class', '1b'))
print(search_by_club(data_students, 'Club', 'Chess'))
print(search_by_gender(data_students, 'Gender', 'W'))
print(search_by_name(data_students, 'Name', 'Hayato'))
